﻿using AutoFixture.AutoMoq;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using System.Collections.Generic;
using Xunit;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class GetPartnersAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;

        public GetPartnersAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }

        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public async void GetPartnersAsyncTests_WhenIsActiveIsFalse_ReturnsModelWithIsActiveFalse(bool isPartnerActive)
        {
            // Arrange
            var autoFixture = new Fixture();

            #region FSetup
            autoFixture.Behaviors.OfType<ThrowingRecursionBehavior>().ToList().ForEach(q => autoFixture.Behaviors.Remove(q));
            autoFixture.Behaviors.Add(new OmitOnRecursionBehavior());
            #endregion

            var partner1 = autoFixture.Build<Partner>()
                .With(q => q.IsActive, isPartnerActive)
                .Create();
            _partnersRepositoryMock.Setup(repo => repo.GetAllAsync()).ReturnsAsync(new List<Partner>() { partner1 });

            // Act
            var response = await _partnersController.GetPartnersAsync();
            var okObjectResult = response.Result as OkObjectResult;
            var list = okObjectResult.Value as IEnumerable<PartnerResponse>;

            //Assert
            list.Single(q => q.Id.Equals(partner1.Id)).Should()
                .Match<PartnerResponse>(q => q.IsActive.Equals(partner1.IsActive));
        }
    }
}